package org.flvc.library;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;

/**
 * Portlet implementation class MyFLVCPortlet
 */
public class MyFLVCPortlet extends MVCPortlet {

	private Log log = LogFactoryUtil.getLog(getClass());
	
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		try {
			if (log.isDebugEnabled()) {
				log.debug("debug message");
			}

			String message = ParamUtil.getString(actionRequest, "message");
			if (message == "hi") {
				log.info("hi");
			} else {
				throw new Exception("Oh no!");
			}
		} catch (Exception e) {
			log.error("my readable exception", e);
		}
	}
}
